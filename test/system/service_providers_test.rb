require "application_system_test_case"

class ServiceProvidersTest < ApplicationSystemTestCase
  setup do
    @service_provider = service_providers(:one)
  end

  test "visiting the index" do
    visit service_providers_url
    assert_selector "h1", text: "Service Providers"
  end

  test "creating a Service provider" do
    visit service_providers_url
    click_on "New Service Provider"

    fill_in "Contact no", with: @service_provider.contact_no
    fill_in "Description", with: @service_provider.description
    fill_in "Email", with: @service_provider.email
    fill_in "Image", with: @service_provider.image
    fill_in "Lowest price", with: @service_provider.lowest_price
    fill_in "Max speed", with: @service_provider.max_speed
    fill_in "Name", with: @service_provider.name
    fill_in "Rating", with: @service_provider.rating
    fill_in "Url", with: @service_provider.url
    click_on "Create Service provider"

    assert_text "Service provider was successfully created"
    click_on "Back"
  end

  test "updating a Service provider" do
    visit service_providers_url
    click_on "Edit", match: :first

    fill_in "Contact no", with: @service_provider.contact_no
    fill_in "Description", with: @service_provider.description
    fill_in "Email", with: @service_provider.email
    fill_in "Image", with: @service_provider.image
    fill_in "Lowest price", with: @service_provider.lowest_price
    fill_in "Max speed", with: @service_provider.max_speed
    fill_in "Name", with: @service_provider.name
    fill_in "Rating", with: @service_provider.rating
    fill_in "Url", with: @service_provider.url
    click_on "Update Service provider"

    assert_text "Service provider was successfully updated"
    click_on "Back"
  end

  test "destroying a Service provider" do
    visit service_providers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Service provider was successfully destroyed"
  end
end
