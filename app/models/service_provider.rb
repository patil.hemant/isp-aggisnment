class ServiceProvider < ApplicationRecord
	#image uploader
	mount_uploader :image, AttachmentUploader
	# self.per_page = 4
  
  #validations
  validates :name ,presence: {message: "Service Provider name is required."}
  validates :name, allow_blank: true, format: {:with => /\A[^`!@#\$%\^*+_=]+\z/, message: "Invalid ISP Name."}
  validates :image, presence: { message: "Service Provider brand image is required." }
  validates :image, allow_blank: true, format: { with: %r{.(jpeg|jpg|bmp|png)\Z}i, message: 'Service Provider brand image must be .jpeg, .jpg , .png or .bmp image' }
	validates :url, url: { allow_blank: true, message: "Please enter valid portal URL" }

	def self.search(params)
		service_providers = ServiceProvider.all
		if params[:search_text].present? and params[:name_or_price_button].present?
			ids1 = service_providers.where('name ILIKE ?', '%' + params[:search_text] + '%').pluck(:id)
			ids2 = service_providers.where('lowest_price = ?', params[:search_text]).pluck(:id) rescue []
			ids = ids1 << ids2
			service_providers = service_providers.where('id IN (?)', ids.flatten)
		end
		service_providers = service_providers.order("rating #{params[:order]}") if params[:sort_by] == "rating" and params[:sort_button].present?
		service_providers = service_providers.order("lowest_price #{params[:order]}") if params[:sort_by] == 'price' and params[:sort_button].present?
		service_providers	
	end
end


