json.extract! service_provider, :id, :name, :lowest_price, :rating, :max_speed, :description, :contact_no, :email, :image, :url, :created_at, :updated_at
json.url service_provider_url(service_provider, format: :json)
