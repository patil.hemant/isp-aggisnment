(function() {
  App.service_provider = App.cable.subscriptions.create("ServiceProviderChannel", {
    connected: function() {},
    disconnected: function() {},
    received: function(data) {
      $('.provider-name').text(data.name)
      $('.provider-lowest-price').text(data.lowest_price)
      $('.provider-max-speed').text(data.max_speed)
      $('.provider-description').text(data.description)
      $('.provider-contact-no').text(data.contact_no)
      $('.provider-email').text(data.email)
      $('.provider-name').text(data.name)
    }
  });
}).call(this);