$(document).ready(function(e){

  $("#rating-form").raty({
		path: "/assets/",
		scoreName: "service_provider[rating]"
	});

	$('.review-rating').raty({
		readOnly: true,
		score: function() {
			return $(this).attr('data-score');
		},
		path: '/assets/'
	});

});