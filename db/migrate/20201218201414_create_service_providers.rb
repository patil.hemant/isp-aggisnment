class CreateServiceProviders < ActiveRecord::Migration[5.2]
  def change
    create_table :service_providers do |t|
      t.text :name
      t.integer :lowest_price
      t.float :rating
      t.text :max_speed
      t.text :description
      t.text :contact_no
      t.text :email
      t.text :image
      t.text :url

      t.timestamps
    end
  end
end
