# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)



20.times do |index|
    ServiceProvider.create!(
            :name=>Faker::ElectricalComponents.electromechanical, 
            :url=>"http://lorempixel.com/200/200",
            :description=> Faker::Lorem.paragraph_by_chars(number: 1000),
            :lowest_price=> [100,200.1000,1400,500,600,400].sample,
            :url => "https://facebook.com" ,
            :contact_no => ["9876756456", "890987678", "9087654345", "6456454545"].sample,
            :rating => [1,2,3,4,5].sample,
            :max_speed => ['1mbps', '2mbps', '3mbps', '4mbps'].sample,
            :image =>  File.open("#{Rails.root}/app/assets/images/#{['m.png','n.png', 'p.png'].sample}")
        )
end