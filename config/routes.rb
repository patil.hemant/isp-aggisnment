Rails.application.routes.draw do
	mount ActionCable.server => '/cable'
  root 'service_providers#index'
  resources :service_providers
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
